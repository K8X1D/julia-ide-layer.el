;;; julia-ide-layer.el -*- lexical-binding: t; -*-
;; Package-Requires: ((emacs "26.1") (julia-mode "0.4") (julia-vterm "0.16") (ob-julia-vterm "0.2") (eglot) (eglot-jl))


;; TODO: set-up forge
;; TODO: complete header


;;; Dependencies
(require 'julia-mode)
(require 'julia-vterm)
(require 'ob-julia-vterm)
(require 'eglot-jl)

;;; Configuration
(eglot-jl-init)

;;; Hooks
(add-hook 'julia-mode-hook 'julia-vterm-mode)
(add-hook 'org-mode-hook (lambda ()
                           (add-to-list 'org-babel-load-languages '(julia-vterm . t))
                           (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)))
(add-hook 'julia-mode-hook #'eglot-ensure)

;;; Provide
(provide 'julia-ide-layer)

;;; julia-ide-layer.el ends here
